import time

from pyspark.dbutils import DBUtils
from pyspark.sql import SparkSession

from mayauth.ssh import MayAuthSSH
from may.rtnutils.rtn_fetching.rtn_fetcher import RtnServerRtnFetcher
from may.rtnutils.rtn_fetching.rtn_cache import _RtnFsCache


def get_spark():
    return SparkSession.builder.getOrCreate()


def get_dbutils():
    return DBUtils(get_spark())


def profile_function(func):
    def wrap(*args, **kwargs):
        started_at = time.time()
        result = func(*args, **kwargs)
        print(f"{func.__name__}: {time.time() - started_at}")
        return result

    return wrap


def create_rtn_fetcher():
    may_auth_ssh_key = get_dbutils().secrets.get(scope="may_auth", key="databricks_may_auth_private_ssh_key")
    may_auth_username = get_dbutils().secrets.get(scope="may_auth", key="databricks_may_auth_username")
    rtn_fetcher = RtnServerRtnFetcher(may_auth_username, may_auth_ssh_key)
    rtn_fetcher.rtn_cache = _RtnFsCache("/dbfs/FileStore/brendan.kerrigan/rtn_files")
    return rtn_fetcher
