import pyspark.sql.types as T

EGO_GEOM_SCHEMA = T.StructType(
    [
        T.StructField("body_width", T.FloatType(), nullable=False),
        T.StructField("body_length", T.FloatType(), nullable=False),
        T.StructField("body_length_offset", T.FloatType(), nullable=False),
    ]
)

BOUNDARY_SEGMENT_SCHEMA = T.StructType(
    [
        T.StructField("id", T.LongType()),
        T.StructField("boundary_pts", T.ArrayType(T.ArrayType(T.FloatType()))),
        T.StructField("typemask", T.LongType()),
        T.StructField("typemask_readable", T.StringType()),
    ]
)

BOUNDARY_SEGMENTS_SCHEMA = T.ArrayType(BOUNDARY_SEGMENT_SCHEMA)

ENVIRONMENT_VARIABLE_SCHEMA = T.StructType(
    fields=[T.StructField(name="MAY_ROUTE_NETWORK_PATH", dataType=T.StringType())]
)

PASSING_EVENT_W_BOUNDARY_SCHEMA_STR = "utime long, log_name string, lateral_distance float, side string, direction string, ego_speed float, object_speed float, relative_speed float, object_id int, is_border_between_agents boolean, ego_dist_to_border float, object_dist_to_border float, boundary_segment_id long"

PASSING_EVENT_SCHEMA_STR = "utime long, log_name string, lateral_distance float, side string, direction string, ego_speed float, object_speed float, relative_speed float, object_id int"

MIN_LATERAL_DIST_PASSING_EVENT_SCHEMA = T.StructType(
    [
        T.StructField("log_name", T.StringType()),
        T.StructField("object_id", T.IntegerType()),
        T.StructField("is_border_between_agents", T.BooleanType()),
        T.StructField("was_boundary_ever_crossed", T.BooleanType()),
        T.StructField("utime", T.LongType()),
        T.StructField("lateral_distance", T.FloatType()),
        T.StructField("side", T.StringType()),
        T.StructField("direction", T.StringType()),
        T.StructField("ego_speed", T.FloatType()),
        T.StructField("object_speed", T.FloatType()),
        T.StructField("relative_speed", T.FloatType()),
        T.StructField("ego_dist_to_border", T.FloatType()),
        T.StructField("object_dist_to_border", T.FloatType()),
        T.StructField("boundary_segment", BOUNDARY_SEGMENT_SCHEMA),
        T.StructField("agent_type", T.StringType()),
        T.StructField("lane_width", T.StringType()),
    ]
)

CACHE_SCHEMA = T.StructType(
    [
        T.StructField("log_name", T.StringType()),
        T.StructField("object_id", T.IntegerType()),
        T.StructField("is_border_between_agents", T.BooleanType()),
        T.StructField("was_boundary_ever_crossed", T.BooleanType()),
        T.StructField("utime", T.LongType()),
        T.StructField("lateral_distance", T.FloatType()),
        T.StructField("side", T.StringType()),
        T.StructField("direction", T.StringType()),
        T.StructField("ego_speed", T.FloatType()),
        T.StructField("object_speed", T.FloatType()),
        T.StructField("relative_speed", T.FloatType()),
        T.StructField("ego_dist_to_border", T.FloatType()),
        T.StructField("object_dist_to_border", T.FloatType()),
        T.StructField("boundary_segment", BOUNDARY_SEGMENT_SCHEMA),
        T.StructField("agent_type", T.StringType()),
        T.StructField("runlevel_int", T.ShortType()),
        T.StructField("lane_width", T.StringType()),
    ]
)

OBJECT_SCHEMA = T.StructType(
    [
        T.StructField("pos", T.ArrayType(T.FloatType())),
        T.StructField("ntypes", T.ArrayType(T.IntegerType())),
        T.StructField("weights", T.ArrayType(T.FloatType())),
    ]
)
