from pyspark.sql import functions as F, Window
from sparkatk.utilities import attach_async_feature

from passing_events.src.utils import get_spark, profile_function


def constrain_df_between_utimes(df, start_utime, end_utime, utime_col="utime"):
    return df.where(F.col(utime_col) > start_utime).where(F.col(utime_col) < end_utime)


def sample_rows_secs_apart(df):
    return df
    return (
        df.withColumn(
            "utime_passed",
            F.col("utime") - F.first("utime").over(Window.orderBy("utime")),
        )
        .withColumn("period", (F.col("utime_passed") / (1000000 / 10)).cast("int"))
        .withColumn(
            "row_number",
            F.row_number().over(Window.partitionBy("period").orderBy("utime")),
        )
        .filter(F.col("row_number") == 1)
    )


@profile_function
def add_run_level_to_df(df, input_dataframes, utime_col="utime"):
    logs_of_interest = df.select("log_name").rdd.flatMap(lambda x: x).collect()

    rl_df = (
        input_dataframes["RUNLEVEL_HEALTH_ANNOTATED"]
        .where(F.col("log_name").isin(logs_of_interest))
        .withColumn("utime_casted", F.col("runlevel.utime"))
        .withColumn("runlevel_casted", F.col("runlevel.runlevel"))
    )

    return (
        df.transform(
            attach_async_feature(
                rl_df,
                "runlevel_casted",
                utime_col,
                "utime_casted",
                partition_by=["log_name"],
            )
        )
        .withColumn("runlevel_int", F.col("runlevel_casted"))
        .drop("runlevel_casted")
    )
