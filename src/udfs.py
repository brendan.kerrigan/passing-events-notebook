import os
import math
import pyspark.sql.types as T
from pyspark.sql.functions import udf

from passing_events.src.schemas import EGO_GEOM_SCHEMA

@udf(EGO_GEOM_SCHEMA)
def get_ego_geom_udf(vehicle_platform):
    """
    Return geometric information about a vehicle platform.
    Typically, this geometry would live in a 'may' .config file

    Args:
      - vehicle_platform(str): The title of a vehicle platform (e.g. "sienna-v1")
    Return:
      (body_width, body_length, body_length_offset)

    """
    if vehicle_platform == "sienna-v1":
        return (2.3220, 5.26624, 1.4106)
    if vehicle_platform == "sienna-v2":
        return (2.3220, 5.26624, 1.4106)
    if vehicle_platform == "sienna-v3":
        return (2.3220, 5.26624, 1.4106)
    raise Exception


@udf(T.StringType())
def extract_rtn_from_rtn_file_path(rtn_file):
    return os.path.basename(rtn_file)[:-4]


@udf(T.FloatType())
def is_close_to_one_another(x, y, object):
    x1 = x
    x2 = object["pos"][0]
    y1 = y
    y2 = object["pos"][1]
    return math.hypot(x2 - x1, y2 - y1)


@udf(T.StringType())
def get_agent_type_udf(object):
    def _map_agent_type_idx_to_string(i):
        if i == 0:
            return "unknown"
        if i == 1:
            return "vaporous"
        if i == 2:
            return "pedestrian"
        if i == 3:
            return "car"
        if i == 4:
            return "truck"
        if i == 5:
            return "motorcycle"
        if i == 6:
            return "cyclist"
        if i == 7:
            return "bus"
        if i == 8:
            return "golf_cart"
        if i == 9:
            return "background"
        if i == 10:
            return "not_detected"
    
    max_type_idx = 0
    max_type_weight = 0
    
    for type_idx in range(object["type_dist"]["ntypes"]):
        type_weight = object["type_dist"]["weights"][type_idx]
        if type_weight > max_type_weight:
            max_type_idx = type_idx
            max_type_weight = type_weight
    
    return _map_agent_type_idx_to_string(max_type_idx)
