import collections
import json
import pyspark.sql.types as T
from pyspark.sql import functions as F, Window
from pyspark.sql.functions import udf

from sparkatk.utilities import attach_async_feature

from may.rtnutils.py_rtn import PyRtn

from may.vehicle_analysis.passing_events import (
    get_passing_events_from_df,
    _OUTPUT_DF_COLUMNS_BASE_VERSION,
    _OUTPUT_DF_COLUMNS_BORDER_VERSION,
)

from passing_events.src.utils import create_rtn_fetcher, profile_function, get_spark
from passing_events.src.schemas import (
    BOUNDARY_SEGMENTS_SCHEMA,
    PASSING_EVENT_W_BOUNDARY_SCHEMA_STR,
    MIN_LATERAL_DIST_PASSING_EVENT_SCHEMA,
    PASSING_EVENT_SCHEMA_STR,
)
from passing_events.src.udfs import (
    get_ego_geom_udf,
    is_close_to_one_another,
    get_agent_type_udf,
)
from passing_events.src.df_utils import (
    constrain_df_between_utimes,
    sample_rows_secs_apart,
)


@profile_function
def _add_boundary_info_to_passing_events(passing_events_df):
    try:
        cached_py_rtns
    except:
        rtn_fetcher = create_rtn_fetcher()
        cached_py_rtns = {}

    superset = {}
    myhash = collections.defaultdict(dict)

    def _process_row(row):
        def _get_pyrtn(row):
            rtn_path = rtn_fetcher.get_rtn(row["rtn"], row["git_commit_hash"])
            key = f"{row['rtn']}{row['git_commit_hash']}"

            if not key in cached_py_rtns:
                cached_py_rtns[key] = PyRtn(rtn_path)

            return cached_py_rtns[key]

        py_rtn: PyRtn = _get_pyrtn(row)

        def _get_lane_boundary_segments_in_local_frame(row):
            def _map_lane_edge_bndry_sum_to_pts(lane_edge_sum):
                new_sum = {}
                l2g = (row["x"], row["y"], row["theta"])
                new_sum["id"] = lane_edge_sum["id"]
                new_sum["boundary_pts"] = []
                new_sum["boundary_pts"].append(
                    py_rtn.convert_global_xyz_to_local_xyz(
                        lane_edge_sum["source_node_summary"]["global_xyz"],
                        l2g,
                    )
                )
                new_sum["boundary_pts"].append(
                    py_rtn.convert_global_xyz_to_local_xyz(
                        lane_edge_sum["target_node_summary"]["global_xyz"],
                        l2g,
                    )
                )

                new_sum["typemask"] = lane_edge_sum["typemask"]
                new_sum["typemask_readable"] = lane_edge_sum["typemask_readable"]
                return new_sum

            def _get_ego_global_xyz(row):
                l2g = (row["x"], row["y"], row["theta"])
                local_xyz = (row["pos_x"], row["pos_y"], 0)
                local_quat = (
                    row["quat_1"],
                    row["quat_2"],
                    row["quat_3"],
                    row["quat_4"],
                )
                return py_rtn.convert_local_xyz_to_global_xyz(local_xyz, local_quat, l2g)

            def _get_lane_width_at_ego_pos(row):
                edge_summaries = py_rtn.get_edge_summaries_by_xyz(_get_ego_global_xyz(row), search_radius_m=0.01)
                print(edge_summaries)
                raise Exception

            lane_edge_sums = py_rtn.get_edge_summaries_by_xyz(_get_ego_global_xyz(row), search_radius_m=4)

            bndry_lane_edges_per_id = {}

            try:
                if lane_edge_sums is not None:
                    for edge_sum in lane_edge_sums:
                        lane_edge_id = edge_sum["lane_edge"]["id"]
                        if lane_edge_id not in bndry_lane_edges_per_id:
                            if edge_sum["lane_map_edge"] is not None:
                                if (
                                    "boundary" in edge_sum["lane_map_edge"]["types"]
                                    or "hard boundary" in edge_sum["lane_map_edge"]["types"]
                                ):
                                    if lane_edge_id not in superset:
                                        superset[lane_edge_id] = _map_lane_edge_bndry_sum_to_pts(edge_sum["lane_edge"])

                                    edge_pts = superset[lane_edge_id]["boundary_pts"]

                                    try:
                                        _ = myhash[tuple(edge_pts[1])][tuple(edge_pts[0])]
                                    except:
                                        myhash[tuple(edge_pts[0])][tuple(edge_pts[1])] = True
                                        bndry_lane_edges_per_id[lane_edge_id] = superset[lane_edge_id]
            except Exception:
                # TODO No lane map edge
                pass

            return list(bndry_lane_edges_per_id.values())

        # TODO Address 'None being returned from here'
        lane_width = 0

        try:
            ego_edge_summary = py_rtn.get_edge_summary_by_nav_edge_id(row["layer_id_nav"])
            lane_width = ego_edge_summary["lane_map_edge"]["width"]["left"] + abs(
                ego_edge_summary["lane_map_edge"]["width"]["right"]
            )
        except Exception:
            # TODO No lane map edge
            pass

        boundary_segments = _get_lane_boundary_segments_in_local_frame(row)

        return boundary_segments, lane_width

    for index, row in passing_events_df.iterrows():
        boundary_segments, lane_width = _process_row(row)
        passing_events_df.at[index, "boundary_segments"] = json.dumps(boundary_segments)
        passing_events_df.at[index, "lane_width"] = lane_width

    return passing_events_df


@profile_function
def get_passing_events_df(log_of_interest, input_dataframes, utime_bounds=None):
    def _sample_rows_with_objects_close_by(df):
        return (
            df.withColumn("object", F.explode("objects"))
            .drop("objects")
            .filter(F.col("object").isNotNull())
            .withColumn(
                "is_object_near",
                is_close_to_one_another(F.col("pos_x"), F.col("pos_y"), F.col("object")) < 10,
            )
            .filter(F.col("is_object_near") == True)
        )

    @udf(T.BooleanType())
    def is_l2g_available(reason):
        if (reason & 2) == 0:
            return True
        else:
            return False

    # specific_row = input_dataframes["FACT_VEHICLE_SHIFT"].where(F.col("log_name") == log_of_interest).first()
    # first_utime_auto_allowed = specific_row["first_utime_allowed_autonomy"]
    # last_utime_auto_allowed = specific_row["last_utime_allowed_autonomy"]

    ego_pose_df = sample_rows_secs_apart(
        input_dataframes["POSE_FILTERED"]
        .select(
            "log_name",
            "utime",
            "source",
            "pos_x",
            "pos_y",
            "vel_x",
            "vel_y",
            "quat_1",
            "quat_2",
            "quat_3",
            "quat_4",
        )
        .where(F.col("log_name") == log_of_interest)
        .where(F.col("source") == "offload")
        .drop("source")
    )

    to_df = (
        input_dataframes["TRACKED_OBJECTS"]
        .select("log_name", "utime", "objects", "nobjects")
        .where(F.col("log_name") == log_of_interest)
        .where(F.col("nobjects") >= 0)
    )

    l2g_df = (
        input_dataframes["L2G"]
        .select("log_name", "utime", "x", "y", "theta")
        .where(F.col("log_name") == log_of_interest)
    )

    allowed_rl_df = (
        input_dataframes["RAW_ALLOWED_RUNLEVEL"]
        .select("log_name", "utime", "reasons")
        .where(F.col("log_name") == log_of_interest)
    )

    if utime_bounds:
        ego_pose_df = constrain_df_between_utimes(ego_pose_df, utime_bounds[0], utime_bounds[1])
        to_df = constrain_df_between_utimes(to_df, utime_bounds[0], utime_bounds[1])
        l2g_df = constrain_df_between_utimes(l2g_df, utime_bounds[0], utime_bounds[1])
        allowed_rl_df = constrain_df_between_utimes(allowed_rl_df, utime_bounds[0], utime_bounds[1])

    ep_per_to_per_l2g = _sample_rows_with_objects_close_by(
        ego_pose_df.transform(attach_async_feature(to_df, ["objects"], "utime", "utime", partition_by=["log_name"]))
    ).drop("is_object_near")

    tmp_col_names = [
        "log_name",
        "utime",
        "pos_x",
        "pos_y",
        "vel_x",
        "vel_y",
        "quat_1",
        "quat_2",
        "quat_3",
        "quat_4",
        "object",
    ]

    ep_per_tos_per_l2g_w_ego_geom_df = (
        ep_per_to_per_l2g.join(
            input_dataframes["FACT_VEHICLE_SHIFT"].select("log_name", "vehicle_platform"), "log_name", "inner"
        )
        .select(*tmp_col_names, "vehicle_platform")
        .withColumn("ego_geom", get_ego_geom_udf(F.col("vehicle_platform")))
        .drop("vehicle_platform")
        .select(*tmp_col_names, F.col("ego_geom.*"))
    )

    per_agent_categories = ["log_name", "object_id"]

    passing_events_df = (
        ep_per_tos_per_l2g_w_ego_geom_df.withColumn("object_id", F.col("object.id"))
        .groupBy(per_agent_categories)
        .applyInPandas(get_passing_events_from_df, PASSING_EVENT_SCHEMA_STR)
        .where((F.abs("lateral_distance") < 2))
        .join(
            ep_per_to_per_l2g.withColumn("object_id", F.col("object.id")), ["utime", "log_name", "object_id"], "inner"
        )
        .withColumn("agent_type", get_agent_type_udf(F.col("object")))
        .transform(attach_async_feature(l2g_df, ["x", "y", "theta"], "utime", "utime", partition_by=["log_name"]))
        .transform(
            attach_async_feature(
                allowed_rl_df,
                ["reasons"],
                "utime",
                "utime",
                partition_by=["log_name"],
            )
        )
        .filter(is_l2g_available(F.col("reasons")))
        .filter(F.col("reasons").isNotNull())
    )

    return passing_events_df.drop(
        *[x for x in _OUTPUT_DF_COLUMNS_BASE_VERSION if x not in ["log_name", "utime", "object_id"]]
    )


@profile_function
def get_min_dist_passing_events_for_log(logs_of_interest, input_dataframes, utime_bounds=None):
    tmp_col_names = [
        "log_name",
        "utime",
        "pos_x",
        "pos_y",
        "vel_x",
        "vel_y",
        "quat_1",
        "quat_2",
        "quat_3",
        "quat_4",
        "object",
        "border_pts",
        "boundary_segment_id",
    ]

    per_agent_categories = ["utime", "log_name", "object_id", "boundary_segment_id"]

    minimum_distance_categories = [
        "log_name",
        "object_id",
        "is_border_between_agents",
        "was_boundary_ever_crossed",
    ]

    is_any_border_present_categories = ["log_name", "object_id", "utime"]

    @profile_function
    def _explode_boundary_segments(df):
        return (
            df.withColumn(
                "boundary_segments",
                F.from_json(
                    df.boundary_segments,
                    BOUNDARY_SEGMENTS_SCHEMA,
                ),
            )
            .withColumn("boundary_segment", F.explode("boundary_segments"))
            .drop("boundary_segments")
            .withColumn("border_pts", F.col("boundary_segment.boundary_pts"))
            .withColumn("boundary_segment_id", F.col("boundary_segment.id"))
        )

    @profile_function
    def _annotate_passing_events_w_boundary_calculations(df):
        x = (
            (
                df.select(
                    *tmp_col_names,
                    "vehicle_platform",
                    "object_id",
                )
                .withColumn("ego_geom", get_ego_geom_udf(F.col("vehicle_platform")))
                .drop("vehicle_platform")
                .select(*tmp_col_names, F.col("ego_geom.*"), "object_id")
                .groupBy(per_agent_categories)
                .applyInPandas(get_passing_events_from_df, PASSING_EVENT_W_BOUNDARY_SCHEMA_STR)
                .join(
                    df,
                    [
                        "log_name",
                        "utime",
                        "object_id",
                        # "lateral_distance",
                        "boundary_segment_id",
                        # *_OUTPUT_DF_COLUMNS_BASE_VERSION,
                    ],
                    "inner",
                )
                .drop("row_number", "min_lateral_distance")
                .select([*_OUTPUT_DF_COLUMNS_BORDER_VERSION, "boundary_segment", "agent_type", "lane_width"])
            )
            .dropDuplicates(
                subset=[
                    "log_name",
                    "utime",
                    "object_id",
                    "is_border_between_agents",
                    "ego_dist_to_border",
                    "object_dist_to_border",
                ]
            )
            .drop("boundary_segment_id", "theta", "x", "y")
        )
        return x

    @profile_function
    def _mark_if_boundary_was_crossed(df):
        return (
            df.withColumn(
                "was_boundary_crossed",
                F.when(
                    F.col("is_border_between_agents")
                    != F.lag("is_border_between_agents").over(
                        Window.partitionBy(["log_name", "object_id", "boundary_segment.id"]).orderBy("object_id")
                    ),
                    True,
                ).otherwise(False),
            )
            .withColumn(
                "was_boundary_ever_crossed",
                F.max("was_boundary_crossed").over(
                    Window.partitionBy(is_any_border_present_categories).orderBy("object_id")
                )
                == True,
            )
            .drop("was_boundary_crossed")
        )

    @profile_function
    def _handle_mixed_border_presence(df):
        """
        If there is a row with and without a border between the agents then toss out the ones without.
        """
        return (
            df.withColumn(
                "border_present_trigger",
                F.when(
                    F.col("is_border_between_agents") == True,
                    True,
                ).otherwise(False),
            )
            .withColumn(
                "is_any_border_between_agents",
                F.max("border_present_trigger").over(
                    Window.partitionBy(is_any_border_present_categories).orderBy("object_id")
                )
                == True,
            )
            .filter(
                ((F.col("is_border_between_agents") == False) & (F.col("is_any_border_between_agents") == True)) != True
            )
            .drop("border_present_trigger", "is_any_border_between_agents")
        )

    @profile_function
    def _reduce_events_to_min_lateral_distance(df):
        # TODO can get rid of the min lateral distance column
        return (
            df.groupBy(minimum_distance_categories)
            .agg(F.min(F.abs("lateral_distance")).alias("min_lateral_distance"))
            .join(df, minimum_distance_categories, "inner")
            .where(F.abs("lateral_distance") == F.col("min_lateral_distance"))
            .withColumn(
                "row_number",
                F.row_number().over(Window.partitionBy(minimum_distance_categories).orderBy("object_id")),
            )
            .where(F.col("row_number") == 1)
            .drop("row_number", "min_lateral_distance")
        )

    route_loc_df = input_dataframes["ROUTE_LOCATION"]

    if utime_bounds:
        route_loc_df = constrain_df_between_utimes(route_loc_df, utime_bounds[0], utime_bounds[1])

    pass_events_ammended = (
        get_passing_events_df(logs_of_interest, input_dataframes, utime_bounds)
        .join(
            input_dataframes["FACT_VEHICLE_SHIFT"].select("log_name", "rtn", "git_commit_hash", "vehicle_platform"),
            on="log_name",
            how="inner",
        )
        .transform(attach_async_feature(route_loc_df, ["layer_id_nav"], "utime", "utime", partition_by=["log_name"]))
    )

    if pass_events_ammended.count() > 0:
        return _reduce_events_to_min_lateral_distance(
            _handle_mixed_border_presence(
                _mark_if_boundary_was_crossed(
                    _annotate_passing_events_w_boundary_calculations(
                        _explode_boundary_segments(
                            get_spark().createDataFrame(
                                _add_boundary_info_to_passing_events(pass_events_ammended.toPandas())
                            )
                        )
                    )
                )
            )
        )
    else:
        return get_spark().createDataFrame([], schema=MIN_LATERAL_DIST_PASSING_EVENT_SCHEMA)
