# Passing Events Generator

This is an experimental tool used to generate and analyze _passing events_.

## Developer setup

### The vehicle analysis package.


Within the `may` repo, checkout `etc/passing-events-v2-backup`.

```
cd ~/may/software/python/
python setup.py bdist_wheel
build-pymay
databricks fs rm dbfs:/FileStore/brendan.kerrigan/may-_package_version_-py3-none-any.whl
databricks fs cp ~/may/software/python/dist/may-_package_version_-py3-none-any.whl dbfs:/FileStore/brendan.kerrigan/ --overwrite
```

### Notebook setup

`cd` in to the root of this project and run: \
`dbx sync workspace --source . -d passing_events`

This command will monitor changes to source files and push them to the Databricks workspace, enabling rapid development.