# Databricks notebook source
"""
Manually installing the 'may' Python package until the changes relevant to this notebook gets through code review
"""

!pip install -vvv --ignore-installed /dbfs/FileStore/brendan.kerrigan/may-_package_version_-py3-none-any.whl

# COMMAND ----------
spark.conf.get("spark.databricks.workspaceUrl")
print(os.getcwd())
# COMMAND ----------

import os
import math
import time
from textwrap import wrap
from pyspark.sql import functions as F
import matplotlib.pyplot as plt
from matplotlib import gridspec

import sys
path = dbutils.notebook.entry_point.getDbutils().notebook().getContext().notebookPath().get()
directory = f"/Workspace/{'/'.join(list(path.split('/')[0:-3]))}"
sys.path.append(directory)
from passing_events.src.schemas import (
    MIN_LATERAL_DIST_PASSING_EVENT_SCHEMA,
    ENVIRONMENT_VARIABLE_SCHEMA,
    CACHE_SCHEMA
)
from passing_events.src.consts import USEC_PER_SEC
from passing_events.src.df_utils import add_run_level_to_df
from passing_events.src.udfs import extract_rtn_from_rtn_file_path
from passing_events.src.passing_events import get_min_dist_passing_events_for_log
 

# COMMAND ----------

dbutils.widgets.dropdown("log_mode", "test_log", ["custom_query", "test_log"])

# COMMAND ----------

# Log Modes
ANALYZE_TEST_LOG = "test_log"
ANALYZE_CUSTOM_QUERY = "custom_query"
LOG_MODE = dbutils.widgets.get("log_mode")

# Parameters for TEST_LOG mode
TEST_LOG = "2023-12-11_mujin_hq/mcmlog_2023-12-11_13-36-18/mcmlog_2023-12-11_13-36-18.mcm"
TEST_LOG_UTIME = 1702320007352066
TEST_LOG_SNIPPIT_WINDOW_SECS = USEC_PER_SEC * 1
TEST_LOG_UTIME_BOUNDS = [TEST_LOG_UTIME - TEST_LOG_SNIPPIT_WINDOW_SECS, TEST_LOG_UTIME + TEST_LOG_SNIPPIT_WINDOW_SECS]
#
NORMAL = True
IN_TEST_MODE = True if LOG_MODE == ANALYZE_TEST_LOG else False

# Caching Configuration
CACHED_RESULTS_PATH = "FileStore/brendan.kerrigan/test_cached_passing_events.parquet" if IN_TEST_MODE else "FileStore/brendan.kerrigan/cached_passing_events.parquet"
READ_PATH = f"dbfs:/{CACHED_RESULTS_PATH}"
WRITE_PATH = f"dbfs:/{CACHED_RESULTS_PATH}"

# COMMAND ----------

if IN_TEST_MODE:
    #pass 
    dbutils.fs.rm(READ_PATH, recurse=True) 

# COMMAND ----------

def cache_file_exists():
    try:
        dbutils.fs.ls(READ_PATH)
        return True
    except:
        return False



# COMMAND ----------

def get_input_dfs():
    input_dataframes = {}
    input_dataframes["SHIFT_INFO"] = spark.table("peavey_stage_clone.log_shiftinfo")
    input_dataframes["FACT_VEHICLE_SHIFT"] = spark.table("peavey_metrics_clone.fact_vehicle_shift")
    input_dataframes["RAW_ALLOWED_RUNLEVEL"] = spark.table("peavey_stage_clone.raw_allowed_runlevel")
    input_dataframes["RUNLEVEL_HEALTH_ANNOTATED"] = spark.table("peavey_stage_clone.silver_runlevel_health_annotated_t")
    input_dataframes["POSE_FILTERED"] = spark.table("peavey_stage_clone.silver_pose_filtered")
    input_dataframes["TRACKED_OBJECTS"] = spark.table("peavey_stage_clone.silver_tracked_objects")
    input_dataframes["L2G"] = spark.table("peavey_stage_clone.silver_l2g")
    input_dataframes["ROUTE_LOCATION"] = spark.table("peavey_stage_clone.raw_route_location")
    return input_dataframes

INPUT_DATA_FRAMES = get_input_dfs()

""" 
Collect logs of interest.
"""

shift_info_df = INPUT_DATA_FRAMES["SHIFT_INFO"].join(
    INPUT_DATA_FRAMES["FACT_VEHICLE_SHIFT"].select("log_name", "autonomy_pct", "km_in_autonomy", "first_utime_allowed_autonomy", "last_utime_allowed_autonomy", "first_utime_overall", "shift_duration_usecs"),
    on="log_name",
    how="inner",
)
 
if LOG_MODE == ANALYZE_TEST_LOG:
    logs_of_interest = [TEST_LOG]
    logs_to_use_df = shift_info_df.where(F.col("log_name").rlike("|".join(logs_of_interest)))
elif LOG_MODE == ANALYZE_CUSTOM_QUERY:
    if NORMAL:
        logs_to_use_df = (
            shift_info_df.orderBy("first_utime_overall", ascending=False)
            #.where(F.col("km_in_autonomy") > 15)
            .where(F.col("site") == "ann-arbor")
            #.where(F.col("git_branch") == "rc/19.4")
            .orderBy("km_in_autonomy", ascending=True).where(F.col("first_utime_allowed_autonomy").isNotNull()).where((F.col("shift_duration_usecs") / 1000000) > 500).where(F.col("vehicle_platform").contains("sienna"))
            .limit(100)
        )
    else:
        logs_to_use_df = (
            shift_info_df.where(F.col("log_name") == "2023-12-11_mujin_hq/mcmlog_2023-12-11_13-36-18/mcmlog_2023-12-11_13-36-18.mcm")
            #.where(F.col("km_in_autonomy") > 15)
            #.where(F.col("site") == "ann-arbor")
            #.where(F.col("git_branch") == "rc/19.4")
            #.orderBy("first_utime_overall", ascending=False).where((F.col("shift_duration_usecs") / 1000000) < 2000)).where((F.col("shift_duration_usecs") / 1000000) > 1000).where(F.col("autonomy_pct") > 0.1).where(F.col("vehicle_platform").contains("sienna")).limit(0)
            #.where(F.col("log_name") == "2023-12-11_mujin_hq/mcmlog_2023-12-11_13-36-18/mcmlog_2023-12-11_13-36-18.mcm")
        )
    logs_to_use_df.display()
else:
    raise Exception

fact_vehicle_shift_df = (
    logs_to_use_df.withColumn(
        "env_variable",
        F.from_json(F.col("env_variable"), ENVIRONMENT_VARIABLE_SCHEMA),
    )
    .where(F.col("env_variable.MAY_ROUTE_NETWORK_PATH").isNotNull())
    .withColumn(
        "rtn", 
        extract_rtn_from_rtn_file_path(F.col("env_variable.MAY_ROUTE_NETWORK_PATH")),
    )
    .select(
        "rtn",
        "log_name",
        "git_branch",
        "git_commit_hash",
        "vehicle_platform",
        "date",
        "first_utime_overall",
        "first_utime_allowed_autonomy",
        "last_utime_allowed_autonomy",
        "km_in_autonomy",
    )
    .orderBy("first_utime_overall", ascending=False)
    .drop("date")
)

INPUT_DATA_FRAMES["FACT_VEHICLE_SHIFT"] = fact_vehicle_shift_df


logs_of_interest = fact_vehicle_shift_df.agg(F.collect_list(F.col("log_name"))).collect()[0][0]

if NORMAL and cache_file_exists():
    logs_of_interest = [
        x for x in logs_of_interest if spark.read.parquet(READ_PATH, header=True, schema=CACHE_SCHEMA).filter(F.col("log_name").contains(x)).count() == 0
    ]

fact_vehicle_shift_df = fact_vehicle_shift_df.where(F.col("log_name").rlike("|".join(logs_of_interest)))

fact_vehicle_shift_df.display()

# COMMAND ----------

i = 0

for log_of_interest in logs_of_interest:
    if cache_file_exists():
        print("The cache exists")
        passing_events_from_file = spark.read.parquet(READ_PATH, header=True, schema=CACHE_SCHEMA)
    else:
        print("Assembling a new cache")
        passing_events_from_file = spark.createDataFrame([], schema=CACHE_SCHEMA)

    if not IN_TEST_MODE:
        combined_results_df = passing_events_from_file
    else:
        combined_results_df = spark.createDataFrame([], schema=CACHE_SCHEMA)

    start_time = time.time()
    print(f"Processing #{i}: '{log_of_interest}'")

    if (not NORMAL) or IN_TEST_MODE:
        utime_bounds = TEST_LOG_UTIME_BOUNDS
    else:
        utime_bounds = None

    min_dist_passing_events_df = get_min_dist_passing_events_for_log(log_of_interest, INPUT_DATA_FRAMES, utime_bounds)
    
    if min_dist_passing_events_df.count() > 0:
        new_passing_events_df = add_run_level_to_df(min_dist_passing_events_df, INPUT_DATA_FRAMES)
    else:
        print("No new events")
        new_passing_events_df = spark.createDataFrame([], CACHE_SCHEMA)

    new_passing_events_df = new_passing_events_df.select(sorted(new_passing_events_df.columns))
    combined_results_df = combined_results_df.select(sorted(combined_results_df.columns))
    combined_results_df = new_passing_events_df.unionAll(combined_results_df)
    combined_results_df.write.option("schema", CACHE_SCHEMA).mode("overwrite").parquet(WRITE_PATH)

    print(f"Processing {log_of_interest} took {time.time() - start_time}")
    i += 1

# COMMAND ----------

###############################
# This was verified using PDV #
###############################

total_passing_events_df = spark.read.parquet(READ_PATH, header=True, schema=CACHE_SCHEMA).join(spark.table("peavey_metrics_clone.fact_vehicle_shift").select("log_name", "first_utime_overall"), "log_name", "inner").withColumn("time_of_occurance_sec", ((F.col("utime")) - F.col("first_utime_overall"))  / 1000000)

total_passing_events_df.where(F.col("runlevel_int").isNotNull()).display()

if LOG_MODE == ANALYZE_TEST_LOG:
    print("Checking test log")
    test_results_df = total_passing_events_df.where(F.col("object_id") == 1071934)
    specific_row = test_results_df.first()
    assert specific_row["is_border_between_agents"]
    assert math.isclose(1.718, specific_row["lateral_distance"], abs_tol=1e-1)
    assert math.isclose(0.340, specific_row["ego_dist_to_border"], abs_tol=1e-1)
    assert math.isclose(-1.37, specific_row["object_dist_to_border"], abs_tol=1e-1)

# COMMAND ----------

total_passing_events_df = (
    total_passing_events_df.where(F.col("was_boundary_ever_crossed") == False)
    .where(F.col("runlevel_int").isNotNull())
    .withColumn(
        "border_type",
        F.when(F.col("is_border_between_agents") == False, "none").otherwise(
            F.col("boundary_segment.typemask_readable")
        ),
    )
)

# COMMAND ----------

total_passing_events_df.where(F.col("direction") == "oncoming").where(
    F.col("agent_type") == "car"
).where(F.col("side") == "right").where(
    F.col("border_type") == "none"
).display()

# COMMAND ----------

pandas_df = total_passing_events_df.toPandas()

# COMMAND ----------

import numpy as np
import matplotlib
import matplotlib.pylab as pl
from matplotlib import pyplot as plt
from matplotlib.colors import ListedColormap
import collections

def makehash():
    return collections.defaultdict(makehash)

def draw_2d_hist(
    axe, x, y, xr=None, yr=None, label="Data", title="Unknown", cmap="Blues"
):
    h = axe.hist2d(
        x.abs(),
        y.abs(),
        bins=100,
        density=True,
        range=[[0, 1], [0, 13]],
        cmap=cmap,
        label=label,
    )
    axe.set_title(title)
    return h


def get_fading_red_color():
    red = pl.cm.Reds  # original colormap
    fading_red = red(np.arange(red.N))  # extract static_colors
    fading_red[:, -1] = np.linspace(0, 1, red.N)  # modify alpha
    fading_red = ListedColormap(fading_red)  # convert to colormap
    return fading_red


def get_fading_color(original_static_colors=pl.cm.Reds):
    color = original_static_colors  # original colormap
    fading_color = color(np.arange(color.N))  # extract static_colors
    fading_color[:, -1] = np.linspace(0, 1, color.N)  # modify alpha
    fading_color = ListedColormap(fading_color)  # convert to colormap
    return fading_color

def split_range_into_chunks(start, end, chunk_size):
    current = start
    while current < end:
        yield (current, min(current + chunk_size, end))
        current += chunk_size

def segment_df(df, axis, bounds):
    return df[(df[axis].abs() > bounds[0]) & (df[axis].abs() < bounds[1])]

static_colors = ["red", "blue", "green", "purple", "orange", "grey"]

fading_colors = [
    get_fading_color(matplotlib.cm.get_cmap("Reds")),
    get_fading_color(matplotlib.cm.get_cmap("Blues")),
    get_fading_color(matplotlib.cm.get_cmap("Greens")),
    get_fading_color(matplotlib.cm.get_cmap("Purples")),
    get_fading_color(matplotlib.cm.get_cmap("Oranges")),
    get_fading_color(matplotlib.cm.get_cmap("Greys")),
]

runlevels = [3, 7]

groups_iter = pandas_df.groupby(
    [
        "direction",
        "side",
        "border_type",
        "agent_type",
    ]
)

ncols = 3
nrows = 3  # groups_iter.ngroups
fig_width = 18
fig_height = 18
fig = plt.figure(figsize=(fig_width, fig_height * nrows))
gs0 = fig.add_gridspec(nrows=nrows, ncols=1, hspace=0.2)

curr_row = 0

for group, group_df in groups_iter:
    if group == ('oncoming', 'right', 'none', 'car') or group == ('oncoming', 'right', 'dotted white', 'car'):
       pass
    else:
       continue

    print(f"{group} {len(group_df)}")
    # if len(group_df) < 50:
    #     len(group_df)
    #     continue
    # if curr_row == nrows:
    #     continue

    gs00 = gridspec.GridSpecFromSubplotSpec(
        3,
        5,
        subplot_spec=gs0[curr_row, :],
        width_ratios=[0.326666, 0.326666, 0.326666, 0.01, 0.01],
        wspace=0.2,
    )

    plots = []
    plots.append(fig.add_subplot(gs00[0, 0]))
    plots.append(fig.add_subplot(gs00[0, 1]))
    plots.append(fig.add_subplot(gs00[0, 2]))
    plots.append(fig.add_subplot(gs00[0, 3]))
    plots.append(fig.add_subplot(gs00[0, 4]))
    plots.append(fig.add_subplot(gs00[1, 0]))
    plots.append(fig.add_subplot(gs00[1, 1]))
    plots.append(fig.add_subplot(gs00[1, 2]))
    plots.append(fig.add_subplot(gs00[2, 0]))
    plots.append(fig.add_subplot(gs00[2, 1]))
    plots.append(fig.add_subplot(gs00[2, 2]))

    #############
    # Row 1     #
    #############

    histograms = []
    for i, x_axis_category in enumerate(
        [
            "lateral_distance",
            "ego_dist_to_border",
            "object_dist_to_border",
        ]
    ):
        x_label = wrap(f"{x_axis_category} (m)", 60)

        plots[i].set_xlabel("\n".join(x_label))
        plots[i].set_ylabel("Relative speed (m/s)")

        for j in range(len(runlevels)):
            curr_runlevel = runlevels[j]
            df_segment_dfed_by_runlevel = group_df[
                group_df["runlevel_int"] == runlevels[j]
            ]
            title = wrap(
                f"'{group[3]}' traveling in the '{group[0]}' direction on the '{group[1]}' side with '{border_str}' between.",
                40,
            )

            histograms.append(
                draw_2d_hist(
                    plots[i],
                    df_segment_dfed_by_runlevel[x_axis_category],
                    df_segment_dfed_by_runlevel["relative_speed"],
                    label=f"RUN_LEVEL = {j}",
                    title="\n".join(title),
                    cmap="Blues" if j == 0 else get_fading_red_color(),
                )
            )

    ##########
    # Row 2   #
    ##########

    velocity_segment_dfs = list(split_range_into_chunks(0, 12, 3))

    color_idx = 0

    boxplot_sets = makehash()

    for velocity_segment_df in velocity_segment_dfs:
        input_df = segment_df(group_df, "relative_speed", velocity_segment_df)

        plots[5].hist2d(
            input_df["ego_dist_to_border"].abs(),
            input_df["object_dist_to_border"].abs(),
            bins=50,
            density=True,
            range=[[0, 1], [0, 1]],
            cmap=fading_colors[color_idx],
        )

        plots[5].set_xlabel("ego_dist_to_border")
        plots[5].set_ylabel("object_dist_to_border")

        boxplot_sets["lateral_distance"][velocity_segment_df] = input_df[
            "lateral_distance"
        ].abs()

        boxplot_sets["ego_dist_to_border"][velocity_segment_df] = input_df[
            "ego_dist_to_border"
        ].abs()

        boxplot_sets["object_dist_to_border"][velocity_segment_df] = input_df[
            "object_dist_to_border"
        ].abs()

        color_idx += 1

    plots[8].boxplot(
        boxplot_sets["lateral_distance"].values(),
        labels=boxplot_sets["lateral_distance"].keys(),
    )
    plots[8].set_ylabel("lateral_distance")

    plots[9].boxplot(
        boxplot_sets["ego_dist_to_border"].values(),
        labels=boxplot_sets["ego_dist_to_border"].keys(),
    )
    plots[9].set_ylabel("ego_dist_to_border")

    plots[10].boxplot(
        boxplot_sets["object_dist_to_border"].values(),
        labels=boxplot_sets["object_dist_to_border"].keys(),
    )
    plots[10].set_ylabel("object_dist_to_border")

    ##########
    # Section #
    ##########
    histograms[0][3].set_clim(0, 1)
    histograms[1][3].set_clim(0, 1)

    plt.colorbar(mappable=histograms[0][3], ax=plots[0], cax=plots[3])
    plt.colorbar(mappable=histograms[1][3], ax=plots[0], cax=plots[4])

    plots[3].set_title("Auto")
    plots[4].set_title("Manual")

    plots[1].set_ylabel("")
    plots[2].set_ylabel("")
    curr_row += 1

#this is a comment left on my computer
plt.show()

# COMMAND ----------


